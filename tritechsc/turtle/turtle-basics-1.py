import turtle
# main function
def main():
	screen = turtle.Screen()
	t = turtle.Turtle()
	#t.tracer(0, 0)
	t.pencolor("#ff0000")#red
	t.fd(50)
	t.lt(45)
	t.pencolor("#00ff00")#green
	t.width(4)
	t.bk(100)
	t.rt(45)
	t.pencolor("#0000ff")#blue
	t.width(10)
	t.goto(0,0)
	t.penup()

	# fill poly start
	t.goto(100,100)
	t.pendown()
	t.pencolor("#00ffff")#cyan
	t.begin_fill()
	t.fd(100)
	t.rt(90)
	t.fd(100)
	t.rt(90)
	t.fd(100)
	t.rt(90)
	t.fd(100)
	t.end_fill()
	
	t.penup()
	t.goto(-100,100)
	t.pendown()
	t.pencolor("#ff00ff")#magenta
	t.fillcolor("#ff7f00")#orange
	t.begin_fill()
	t.fd(100)
	t.rt(90)
	t.fd(100)
	t.rt(90)
	t.fd(100)
	t.rt(90)
	t.fd(100)
	t.end_fill()
	t.circle(10)
	t.pencolor("#ffff00")
	t.circle(20)
	t.pendown()
	t.goto(-200,-200)
	t.fillcolor("#00cc7f")
	t.begin_fill()
	t.circle(30)
	t.end_fill()
	
	screen.exitonclick()	
	

if __name__ == "__main__":
		main()
