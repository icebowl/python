#/usr/bin/env python

def binary_search(blist,n):
	first=0
	last=len(blist)-1
	while(first<=last):
		mid=(last+first)//2
		if(blist[mid]==n):
			return mid
		elif(n<blist[mid]):
			last=mid-1
		elif(n>blist[mid]):
			last=mid+1
	return -1

def binarySearch(array, x, low, high):

    # Repeat until the pointers low and high meet each other
    while low <= high:
        mid = low + (high - low)//2
        if array[mid] == x:
            return mid
        elif array[mid] < x:
            low = mid + 1
        else:
            high = mid - 1

    return -1


array = [3, 4, 5, 6, 7, 8, 9]
x = 8

result = binarySearch(array, x, 0, len(array)-1)

if result != -1:
    print("Element is present at index " + str(result))
else:
    print("Not found")
