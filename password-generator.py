import random
import string

def get_random_string(length):
	letters = string.ascii_lowercase
	result_str = ''.join(random.choice(letters) for i in range(length))
	return result_str

def get_random_numbers(length):
	letters = string.digits 
	result_str = ''.join(random.choice(letters) for i in range(length))
	return result_str

for n in range (0,40):
	pass1 = get_random_string(4)
	pass2 = get_random_numbers(4)
	thepassword = pass1+pass2
	print(thepassword)
