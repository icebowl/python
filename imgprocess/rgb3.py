from PIL import Image

def hexcon(num):
	key = "0123456789abcdef" # hex key
	h = ""
	h16 = int(num/16)
	h1 = num % 16
	h = key[h16]+ key[h1]
	return h

def rgb_of_pixel(img_path, x, y):
    im = Image.open(img_path).convert('RGB')
    r, g, b = im.getpixel((x, y))
    a = (r, g, b)
    return r,g,b


def main():
	img = "coleman1.png"
	for y in range (0,80):
		for x in range (0,80):
			r,g,b = rgb_of_pixel(img, x, y)
			#print(r,g,b," ",end="")
			rhex = hexcon(r); ghex = hexcon(g); bhex = hexcon(b)
			hexval = rhex+ghex+bhex
			cathexval = "#"+hexval
			print (cathexval)
			
main()


