import turtle
import time

def poly(t,x,y,size,side,color):

	t.pencolor(color)
	t.fillcolor(color)
	t.begin_fill()
	for n in range (0,4):
		t.forward(size)
		t.left(360/side)
	t.end_fill()

	turtle.update()

def matrix(t,x,y,):
	# color list
	hdl = open("coleman.txt", 'r')
	clhex = hdl.readlines()
	hdl.close()

	for n in range (0,len(clhex)):
		print(clhex[n],end="")
	print(clhex)
	print(t,x,y)
	t.width(1)
	count = 0
	for k in range(0,80):
		b = y - k
		for h in range(0,80):
			a = x + h
			#substring = milist[n][0:7]
			color = clhex[count][0:7]
			print(x,y)
			t.penup()
			t.goto(a+h*10,b-k*10)
			t.pendown()
			poly(t,a,b,10,4,color)
			count = count + 1
			


def main():
	w = turtle.Screen()
	# setup the screen size
	w.setup(900,900)
  
# set the background color
	w.bgcolor("#000011")
	x = -200; y = 0
	w.clear()
	w.bgcolor("#ffffff")
	t = turtle.Turtle()

	turtle.tracer(0, 0)
	matrix(t,-420,420)

	w.exitonclick()
	
if __name__ == '__main__':
	main()

'''
cl = ["#f8f8f8","#e8e8e8" ,"#d8d8d8" ,"#b8b8b8" ,
				"#585858" ,"#383838" ,"#282828" , "#181818" ,
				"#ab4642" ,	"#dc9656" ,"#f79a0e" ,"#538947" ,
				"#4b8093" ,"#7cafc2" ,"#96609e" ,"#a16946" ]

'''
