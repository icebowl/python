a = [[34,38,50,44,39],
     [42,36,40,43,44],
     [24,31,46,40,45],
     [43,47,35,31,26],
     [37,28,20,36,50]]

for r in range(len(a)):
    for c in range(len(a[r])):
        if (a[r][c] % 3 != 0):
            a[r][c] = 0

for i in range(len(a)):
    for j in range(len(a[i])):
        print(a[i][j], end=" ")
    print('')
