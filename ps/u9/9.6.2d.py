def printArray(a):
  for i in range(len(a)):
      for j in range(len(a[i])):
          print(a[i][j], end = " ")
      print()

def reverse(a):
  temp = [] #empty array that we will append to
  for i in range(len(a)):
      #add from end to beginning of original array
      temp.append(a[len(a) -1 -i])
  return temp

#flipping horizontally means leaving order of arrays, but reversing items in each array
def flipHorizontal(a):
  temp = a
  for i in range(len(a)):
      temp[i] = reverse(a[i])#assign each reversed array to the new array
  printArray(temp)

#flipping vertically involves only reversing the order of arrays
def flipVertical(a):
  printArray(reverse(a))

'''MAIN'''
N = [[0, 2, 0, 0, 0], [0, 2, 0, 0, 0], [0, 2, 2, 0, 0], [0, 2, 0, 2, 0], [0, 2, 0, 0, 2]]

printArray(N)
print()
flipHorizontal(N)
print()

N = [[0, 2, 0, 0, 0], [0, 2, 0, 0, 0], [0, 2, 2, 0, 0], [0, 2, 0, 2, 0], [0, 2, 0, 0, 2]]

flipVertical(N)
