#define N array
N = []

def printIt(lst):
    # r(row)  c(column)
    for r in range(len(lst)):
        for c in range(len(lst[0])):
            print(lst[r][c], end=" ")
        print("")
       
#main
for r in range(4):
    N.append([])
    print(N) #debug

for r in range(len(N)):
    value = 1
    for c in range(5):
        N[r].append(value)
        value = value + 1

printIt(N)

print("")

newValue = 1
for r in range(len(N)):
    for c in range(len(N[0])):
        N[r][c] = newValue
    newValue = newValue + 1

printIt(N)
