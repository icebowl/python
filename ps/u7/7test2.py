def mystery1(x):
    return x + 2

def mystery2(a, b = 7):
    return a + b

#MAIN
n = int(input("Enter a number:"))
ans = mystery1(n) * 2 + mystery2 (n * 3)
print(ans)
