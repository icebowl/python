def GPAcalc(g, w):
    if g == "A" or g == "a":
        return 4 + w
    elif g == "B" or g == "b":
        return 3 + w
    elif g == "C" or g == "c":
        return 2 + w
    elif g == "D" or g == "d":
        return 1 + w
    elif g == "F" or g == "f":
        return 0 + w
    else:
        return "Invalid"

lettergrade = input("Enter your letter grade:  ")
weight = int(input("Is it weighted? (1 = yes, 0 = no)"))
gpa = GPAcalc(lettergrade, weight)
print("Your GPA score is: " + str(gpa))
