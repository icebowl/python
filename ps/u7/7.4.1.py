kdef GPAcalc(g):
    if g == "A" or g == "a":
        return 4
    elif g == "B" or g == "b":
        return 3
    elif g == "C" or g == "c":
        return 2
    elif g == "D" or g == "d":
        return 1
    elif g == "F" or g == "f":
        return 0
    else:
        return "Invalid"

letter_grade = input("Enter your letter grade:  ")
gpa = GPAcalc(letter_grade)
print("Your GPA score is: " + str(gpa))
