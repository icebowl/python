def GPAClass(x):
    gpa = 0
    for i in range(0,x):
        grade = input("Enter your letter grade: ")
        weight = int(input("Is it weighted? (1= yes, 0= no) "))
        gpa = gpa + GPAcalc(grade, weight)
    return float(gpa/x)

def GPAcalc(x, y):                  
    x = x.upper()
    if x == 'A':
        print("Your GPA score is: ", 4+y)
        return 4+y
    elif x == 'B':
        print("Your GPA score is: ", 3+y)
        return 3+y
    elif x == 'C':
        print("Your GPA score is: ", 2+y)
        return 2+y
    elif x == 'D':
        print("Your GPA score is: ", 1+y)
        return 1+y
    elif x == 'F':
        print("Your GPA score is: ", 0+y)
        return 0+y
    else:
        return 'Invalid'

gpa = 0
classes = int(input("How many classes are you taking?"))
print("Your weighted GPA score is: " + str(GPAClass(classes)))
