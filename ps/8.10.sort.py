def printIt (a):
    for i in range (len(a)):
        print(a[i])

def sort (a):
    for i in range (len(a)-1):
        temp = a[i]
        sm = i + 1
    
        for j in range (i,len(a)):
            if (a[sm] > a [j]):
                sm = j
        a[i] = a[sm]
        a[sm] = temp

# *********MAIN**************

tests = [89,72,73,90]

printIt (tests)

print("\nIn Order: ")
sort(tests)
printIt(tests)
