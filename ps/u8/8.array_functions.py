numbers = [8,3,5,10]
numbers.sort(reverse=True)
print(numbers)

letters = ["cat","dog","bear","bird"]
letters.sort()
print(letters)

print(" pop ")
letters = ["a","a","b","c"]
print(letters.pop(0))

print(" pop ")
letters = ["$","a","b","c"]
letters.pop (0)
print(letters)

letters = ["a","a","b","c"]
letters.remove("a")
print(letters)

letters = ["a","b","c"]
letters.insert (0,"$")
print(letters)

letters = ["a","b","c"]
letters.extend(["d","e","f"])
print(letters)


