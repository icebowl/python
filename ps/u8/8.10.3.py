def swap (lst, a, b):
    temp = lst[a]
    lst[a] = lst[b]
    lst[b] = temp

# main 

terms = ["Bandwidth", "Hierarchy", "IPv6", "Software", "Firewall", "Cybersecurity", "Lists", "Program", "Logic", "Reliability"]
print (terms)

for i in range(len(terms)):
    for j in (range(i, len(terms))):
        if (terms[i] > terms [j]):
            swap(terms, i, j)

print (terms)

'''
#buble sort
 
def swap(lst,a,b):
	tmp = lst[a]
	lst[a] = lst[b]
	lst[b] = tmp
 
def bubbleSort(lst):
	n = len(lst)
	for i in range(0,n-1):
		for j in range(i, n):
			if lst[i] > lst[j]:
				swap(lst,i,j)
 
def main():
	lst = [99,64, 34, 25, 12, 22, 11]
	print(lst)
	bubbleSort(lst)
	print(lst)
	
main()


'''

