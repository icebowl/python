vocab = ["Libraries", "Bandwidth", "Hierarchy", "Software", "Firewall", "Cybersecurity","Phishing", "Logic", "Productivity"]

print(vocab)

for i in range(len(vocab) -1):
    small = i 
    for j in range (i + 1, len(vocab)):
        if (vocab[small] > vocab[j]):
            small = j
    temp = vocab[small]
    vocab[small] = vocab[i]
    vocab[i] = temp

print(vocab)
