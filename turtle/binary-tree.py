import turtle
import math
import random

def hexcon(num):
	key = "0123456789abcdef" # hex key
	h = ""
	h16 = int(num/16)
	h1 = num % 16
	h = key[h16]+ key[h1]
	return h

def tree(t,sz, level,angle):
	
	if level > 0:
		# drawing the base
		t.fd(sz)
		t.rt(angle)
		# recursive call for  the right subtree
		tree(t,0.75 * sz, level-1,30)
		t.lt( 2 * angle )
		# recursive call for  the left subtree
		tree(t,0.75 * sz, level-1,30)
		t.rt(angle)
		t.fd(-sz)


def tree_setup():
	try:
		turtle.TurtleScreen._RUNNING = True
		w = turtle.Screen()
		w.title("Julia Set")
		w.setup(800, 800)
		w.clear()
		t = turtle.Turtle()
		#t.speed(0)
		turtle.tracer(0, 0)
		t.hideturtle()
		#t.penup()
		#red = 255;green = 0; blue = 0
		red = random.randint(0,255)
		green = random.randint(0,255)
		blue = random.randint(0,255)
		redHex = hexcon(red); greenHex = hexcon(green); blueHex = hexcon(blue)
		thecolor = "#"+redHex+greenHex+blueHex
		t.color(thecolor)
		t.penup()
		t.goto(0,370)
		t.pendown()
		
		style = ('Arial', 12, 'bold')
		t.write('Binary Tree', font=style, align='center')
		t.penup()
		t.goto(0,0)
		t.pendown()
		t.seth(0)
		tree(t,100,11,30)
		t.penup()
		t.goto(0,0)
		t.pendown()
		t.seth(90)
		tree(t,100,11,30)
		t.penup()
		t.goto(0,0)
		t.pendown()
		t.seth(180)
		tree(t,100,11,30)
		t.penup()
		t.goto(0,0)
		t.pendown()
		t.seth(270)
		tree(t,100,11,30)
		w.exitonclick()
	finally:
		turtle.Terminator()

def main():
	tree_setup()
	
if __name__ == "__main__":
	main()

'''
	t.goto(-255,y)
		t.color(#ff0000)
		t.down()
		#t.goto(255,y)
		if (y % 10 == 0):
			t.penup()
			#t.goto(0,y)
			t.color("#000000")
			#t.circle(10)
		y = y + 1


'''
